var Location = function(){
    this.lat = -1;
    this.long = -1;
    this.time = -1;
};

Location.prototype.fillData = function (lat, long, time) {
      if(!lat || !long || !time){
          return false;
      }
    
    this.lat = lat;
    this.long = long;
    this.time = time;
    
    return true;
    };

Location.prototype.createNewPoint = function(){
  return new Location();  
};

module.exports = new Location();