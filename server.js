const path = require('path');
const express = require('express');
const logger = require('morgan');
const low = require('lowdb');
const FileSync = require('lowdb/adapters/FileSync');
const bodyParser = require('body-parser');
const favicon = require('serve-favicon');

const lm = require('./location_manager.js');
const utils = require('./utils.js');
const config = require('./config');

const adapter = new FileSync('locations.json')

const locodb = low(adapter);
locodb.defaults({points: []}).value();
const points = locodb.get('points');

Array.prototype.append = function (array) {
    this.push.apply(this, array)
}

const sharedFilesPath = __dirname + '/public';
const serverFilesPath = __dirname;

const app = express();
app.use(bodyParser.urlencoded({extended: false}));

// Log the requests
app.use(logger('dev'));

// Serve static files
app.use(express.static(sharedFilesPath));
app.use(favicon(path.join(sharedFilesPath,'images','gpsicon.png')));
// Allow origin
app.all('*', function (req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE, OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-Type');
	res.header('Cache-control', 'no-cache');
    next();
});

app.get('/favicon.ico', function(req, res) {
    res.send(204);
});

app.get('/', function (req, res) {
    res.sendFile(path.join(sharedFilesPath + '/map.html'));
});

app.get('/test', function (req, res) {
    res.sendFile(path.join(sharedFilesPath + '/testpage.html'));
});

// Route for everything else
app.get('/show_map', function (req, res) {
    res.sendFile(path.join(sharedFilesPath + '/map.html'));
});

app.get('/get_points', function (req, res) {
    //?time=333
    res.sendFile(path.join(serverFilesPath + '/locations.json'));
    return;
});

app.get('/download', function(req, res){
  var file = __dirname + '/locations.json';
  res.download(file); // Set disposition and send it.
});

app.get('/reset_points', function (req, res) {
    locodb.set('points', []).value();
    return;
});

app.get('/log', function(req, res){
    //?lat=34&longitude=44&time=2
    var lat = req.query.lat;
    var long = req.query.longitude;
    var time = req.query.time;

    if (!lat) {
        res.status(400).send({message: 'Missing lat'});
        return;
    }

    if (!long) {
        res.status(400).send({message: 'Missing long'});
        return;
    }

    if (!time) {
        res.status(400).send({message: 'Missing time'});
        return;
    }

    console.log("message:"+" lat "+lat+" long "+long+" time "+time);

    var locationObject = lm.createNewPoint();
    if(!locationObject.fillData(lat, long, time)){
       console.log("Failed to create locationObject");
       return;
    }

    points.push(locationObject).write();
    res.status(200).send();
    return;
});

// Fire it
app.listen(config.server.port);
console.log('Listening on port ' + config.server.port);