class Point {

    constructor(lat, long, time) {
        if (!lat || !long || !time) {
            this.lat = -1;
            this.long = -1;
            this.time = -1;
            return;
        }

        this.latStr = lat;
        this.longStr = long;

        this.lat = Number(lat);
        this.long = Number(long);

        this.time = time;
    }

    convertToGoogleMarker(map){
        var myLatLng = new google.maps.LatLng(this.lat, this.long);
        var imgUrl = "../images/info.png";

        var markerIcon = {
            size: new google.maps.Size(16, 16),
            anchor: new google.maps.Point(8,8),
            url: imgUrl
        };

        var marker = new google.maps.Marker({
            position: myLatLng,
            map: map,
            icon: markerIcon,
            title: this.time
          });
          
        return marker;
    }

    convertToGoogleLatLng(){
        return new google.maps.LatLng(this.lat, this.long);
    }
}