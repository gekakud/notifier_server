function initMap() {

  var markers = new Array(0);
  var dataPoints = new Array(0);
  var route = new Array(0); 

  var initialPoint = new google.maps.LatLng(31.5, 35.0);
  var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 8,
    center: initialPoint
  });

  var clusterImagesPath = 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m';
  var mapBounds = new ViewPort(map);
  // Add a marker clusterer to manage the markers.
  var markerCluster = new MarkerClusterer(map, markers, {
    imagePath: clusterImagesPath
  });

  var poly = new google.maps.Polyline({
    path: route,
    geodesic: true,
    strokeColor: '#FF2266',
    strokeOpacity: 1.0,
    strokeWeight: 2
  });

  function clearAll() {

    for (var i = 0; i < markers.length; i++) {
      markers[i].setMap(null);
    }

    dataPoints = [];
    markers = [];
    route = [];
    markerCluster.clearMarkers();
    poly.setMap(null);
  }

  function createPoliline(dataPoints) {
    dataPoints.forEach(element => {
      route.push(element.convertToGoogleLatLng())
    });

    poly = new google.maps.Polyline({
      path: route,
      geodesic: true,
      strokeColor: '#FF2266',
      strokeOpacity: 1.0,
      strokeWeight: 2
    });

    poly.setMap(map);
  }

 $(document).ready(function () {

    $("#get_points").click(function () {
//{ version: "5", language: "php" }
      clearAll();
      $.get(testEnd,{time:"2018-11-24T15:46:18.152Z"})
        .done(function (jsonResponse) {
          dataPoints = [];
          for (var i = 0; i < jsonResponse.points.length; i++) {
            var dataPoint = new Point(jsonResponse.points[i].lat, jsonResponse.points[i].long_, jsonResponse.points[i].time);
            dataPoints.push(dataPoint);
            markers[i] = dataPoint.convertToGoogleMarker(map);
          }

          mapBounds.updateView(dataPoints);

          if ($("#show_clusters_checkbox").is(':checked')) {
            markerCluster = new MarkerClusterer(map, markers, {
              imagePath: clusterImagesPath
            });
          }
          createPoliline(dataPoints);
        })
        .fail(function (jqxhr, textStatus, error) {
          var err = textStatus + ", " + error;
          alert("Request Failed: " + err);
        });
    });
//zqIiU$t,toy1e@j
  //   $('#sandbox-container .input-daterange').datepicker({
  //     clearBtn: true
  // });
    $("#clear_all").click(function () {
      clearAll();
    });
    
  });
}
