class ViewPort {

    constructor(map) {
        this.map = map;
        this.minLng = MAX_COORDINATE;
        this.minLat = MAX_COORDINATE;
        this.maxLng = -MAX_COORDINATE;
        this.maxLat = -MAX_COORDINATE;
    }

    updateView(dataPoints) {

        this.minLat = dataPoints[0].lat;
        this.maxLat = dataPoints[0].lat;
        this.minLng = dataPoints[0].long;
        this.maxLng = dataPoints[0].long;
    
    for (var i = 1; i < dataPoints.length; i++) {
        if(this.minLat > dataPoints[i].lat) {
            this.minLat = dataPoints[i].lat;
        }
        if(this.maxLat < dataPoints[i].lat) {
            this.maxLat = dataPoints[i].lat;
        }
        
        if (this.minLng > dataPoints[i].long) {
            this.minLng = lngArr[i].long;
        }
        if (this.maxLng < dataPoints[i].long) {
            this.maxLng = dataPoints[i].long;
        }
    }

        var topRight = new google.maps.LatLng(this.maxLat, this.maxLng);
        var bottomLeft = new google.maps.LatLng(this.minLat, this.minLng);

        var viewPortBoundBox = new google.maps.LatLngBounds(bottomLeft, topRight);

        this.map.fitBounds(viewPortBoundBox);
    }
}